import {
	useFloating,
	useClick,
	useInteractions,
	FloatingArrow,
	arrow,
	offset,
	shift,
} from "@floating-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
	faSquareFacebook,
	faTwitter,
	faPinterest,
} from "@fortawesome/free-brands-svg-icons";
import IconShare from "./IconShare";
import "./Share.scss";
import { useRef, useState } from "react";

const Tooltip = () => {
	const [isOpen, setIsOpen] = useState(false);

	const arrowRef = useRef(null);
	const { refs, floatingStyles, context } = useFloating({
		placement: "top",
		open: isOpen,
		onOpenChange: setIsOpen,
		middleware: [offset(28), arrow({ element: arrowRef })],
	});

	const click = useClick(context);

	const { getReferenceProps, getFloatingProps } = useInteractions([click]);

	return (
		<>
			<a
				href="#"
				className="article__share"
				ref={refs.setReference}
				{...getReferenceProps()}
			>
				<IconShare color={"#6E8098"} />
			</a>
			{isOpen && (
				<div
					className="share"
					ref={refs.setFloating}
					style={floatingStyles}
					{...getFloatingProps()}
				>
					<div className="share__social">
						<p className="share__title">SHARE</p>
						<div className="share__icons">
							<a
								href="#"
								className="share__icons-facebook"
							>
								<FontAwesomeIcon icon={faSquareFacebook} />
							</a>
							<a
								href="#"
								className="share__icons-twitter"
							>
								<FontAwesomeIcon icon={faTwitter} />
							</a>
							<a
								href="#"
								className="share__icons-pinterest"
							>
								<FontAwesomeIcon icon={faPinterest} />
							</a>
						</div>
					</div>
					<a
						href="#"
						className="share__share"
						onClick={() => handleShareBar()}
					>
						<IconShare color={"#FFF"} />
					</a>
					<FloatingArrow
						ref={arrowRef}
						context={context}
						width={24}
						height={12}
						fill="#48556A"
					/>
				</div>
			)}
		</>
	);
};

export default Tooltip;
