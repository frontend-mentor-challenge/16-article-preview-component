import IconShare from "./IconShare";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
	faSquareFacebook,
	faTwitter,
	faPinterest,
} from "@fortawesome/free-brands-svg-icons";

import "./Share.scss";

const Share = ({ handleShareBar, isVisible }) => {
	console.log(isVisible);
	return (
		<div className={`share ${isVisible ? "visible" : ""}`}>
			<div className="share__social">
				<p className="share__title">SHARE</p>
				<div className="share__icons">
					<a
						href="#"
						className="share__icons-facebook"
					>
						<FontAwesomeIcon icon={faSquareFacebook} />
					</a>
					<a
						href="#"
						className="share__icons-twitter"
					>
						<FontAwesomeIcon icon={faTwitter} />
					</a>
					<a
						href="#"
						className="share__icons-pinterest"
					>
						<FontAwesomeIcon icon={faPinterest} />
					</a>
				</div>
			</div>
			<a
				href="#"
				className="share__share"
				onClick={() => handleShareBar()}
			>
				<IconShare color={"#FFF"} />
			</a>
		</div>
	);
};

export default Share;
