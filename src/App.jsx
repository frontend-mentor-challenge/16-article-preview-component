import { useRef, useState } from "react";
import "./App.scss";
import Share from "./Share";
import Avatar from "./assets/images/avatar-michelle.jpg";
import IconShare from "./IconShare";
import Tooltip from "./Tooltip";

function App() {
	const [isVisible, setIsVisible] = useState(false);

	const handleShareBar = () => {
		setIsVisible(!isVisible);
	};

	let screenWidth = window.innerWidth;

	// let width;

	// window.addEventListener("resize", () => {
	// 	width = window.innerWidth;
	// 	console.log(width);
	// });

	return (
		<>
			<article className="article">
				<div className="article__cover"></div>
				<div className="article__content">
					<h1 className="article__tagline">
						Shift the overall look and feel by adding these
						wonderful touches to furniture in your home
					</h1>
					<p className="article__description">
						Ever been in a room and felt like something was missing?
						Perhaps it felt slightly bare and uninviting. I’ve got
						some simple tips to help you make any room feel
						complete.
					</p>
					<div className="article__author">
						<div className="article__author-content">
							<div className="article__image-container">
								<img
									src={Avatar}
									alt="Author Picture"
								/>
							</div>
							<div className="article__author-data">
								<p className="article__author-name">
									Michelle Appleton
								</p>
								<p className="article__author-date">
									28 Jun 2020
								</p>
							</div>
						</div>
						{screenWidth > 768 ? (
							<Tooltip />
						) : (
							<a
								href="#"
								className="article__share"
								onClick={handleShareBar}
							>
								<IconShare color={"#6E8098"} />
							</a>
						)}
					</div>
					{isVisible && (
						<Share
							handleShareBar={handleShareBar}
							isVisible={isVisible}
						/>
					)}
				</div>
			</article>
		</>
	);
}

export default App;
