# Frontend Mentor - Article preview component solution

This is a solution to the [Article preview component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/article-preview-component-dYBN_pYFT). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

-   [Overview](#overview)
    -   [The challenge](#the-challenge)
    -   [Screenshot](#screenshot)
    -   [Links](#links)
-   [My process](#my-process)
    -   [Built with](#built-with)
    -   [What I learned](#what-i-learned)
    -   [Continued development](#continued-development)
    -   [Useful resources](#useful-resources)
-   [Author](#author)
-   [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

-   View the optimal layout for the component depending on their device's screen size
-   See the social media share links when they click the share icon

### Screenshot

![Mobile](./screenshots/mobile.jpg)
![Mobile Active](./screenshots/mobile_active.jpg)
![Desktop](./screenshots/desktop.jpg)
![Desktop Active](./screenshots/desktop_active.jpg)

### Links

-   Solution URL: [Gitlab Repository](https://gitlab.com/frontend-mentor-challenge/16-article-preview-component)
-   Live Site URL: [Live](https://article-preview-component-dani.onrender.com)

## My process

### Built with

-   Semantic HTML5 markup
-   CSS custom properties
-   Flexbox
-   CSS Grid
-   Mobile-first workflow
-   [React](https://reactjs.org/) - JS library
-   [Normalize](https://necolas.github.io/normalize.css/) - CSS Reset
-   [Google Fonts](https://fonts.google.com/)
-   [Font Awesome](https://fontawesome.com/) - Brand Icons
-   [SASS](https://sass-lang.com/) - SASS
-   [Floating UI](https://floating-ui.com/) - Tooltip

### What I learned

### Continued development

### Useful resources

-   [Figma](https://figma.com) - For design
-   [React](https://es.react.dev/reference/react) - For resolving doubts
-   [Floating UI](https://floating-ui.com/) - Tooltip

## Author

-   Website - [Issac Leyva](In construction)
-   Frontend Mentor - [@issleyva](https://www.frontendmentor.io/profile/issleyva)

## Acknowledgments
